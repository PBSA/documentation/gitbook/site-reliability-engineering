---
description: Covered Services/URLs under the SLA
---

# Covered Services

This page provides a comprehensive list of all the services and corresponding URLs that fall within the scope of the Log Aggregation Service Level Agreement (SLA). The SLA defines the commitments, expectations, and quality standards for these services, ensuring that they meet the desired level of performance, availability, and reliability.

By maintaining a clear and up-to-date record of the services and URLs covered under the SLA, we establish transparency and facilitate effective communication with stakeholders. This list serves as a reference point to identify the specific systems, applications, or endpoints that are subject to the SLA and outlines the agreed-upon service levels for each of them.

In this section, you will find an overview of the covered services/URLs:

<table><thead><tr><th>Service</th><th>URL</th><th width="216">Job Value</th><th>Availalbility</th></tr></thead><tbody><tr><td>Grafana Logs</td><td><a href="https://grafana.peerplays.download/expplore">Grafana</a></td><td>N/A</td><td>99.9</td></tr><tr><td>Loki Ingestor</td><td><a href="https://loki.peerplays.download/metrics">Loki</a></td><td>N/A</td><td>99.9</td></tr><tr><td>Production NEX</td><td><a href="https://grafana.peerplays.download/explore">Grafana</a></td><td>production-nex-output</td><td>99.9</td></tr><tr><td>Production NEX</td><td><a href="https://grafana.peerplays.download/explore">Grafana</a></td><td>production-nex-errors</td><td>99.9</td></tr><tr><td>Alphanet NEX</td><td><a href="https://grafana.peerplays.download/explore">Grafana</a></td><td>alphanet-nex-output</td><td>99.9</td></tr><tr><td>Alphanet NEX</td><td><a href="https://grafana.peerplays.download/explore">Grafana</a></td><td>production-nex-output</td><td>99.9</td></tr><tr><td>Alphanet Blockchain</td><td><a href="https://grafana.peerplays.download/explore">Grafana</a></td><td>alphanet-blockchain-logs</td><td>99.9</td></tr><tr><td>Alphanet Faucet</td><td><a href="https://grafana.peerplays.download/explore">Grafana</a></td><td>alphanet-faucet-logs</td><td>99.9</td></tr><tr><td>Devnet NEX</td><td><a href="https://grafana.peerplays.download/explore">Grafana</a></td><td>devnet-nex-output</td><td>99.9</td></tr><tr><td>Devnet NEX</td><td><a href="https://grafana.peerplays.download/explore">Grafana</a></td><td>devnet-nex-errors</td><td>99.9</td></tr><tr><td>Devnet Blockchain</td><td><a href="https://grafana.peerplays.download/explore">Grafana</a></td><td>devnet-blockchain-logs</td><td>99.9</td></tr><tr><td>Devnet Faucet</td><td><a href="https://grafana.peerplays.download/explore">Grafana</a></td><td>devnet-faucet</td><td>99.9</td></tr></tbody></table>

By referring to this list, stakeholders can gain a clear understanding of the services and URLs covered by this SLA and the associated service levels that have been committed to. This promotes transparency, aligns expectations, and ensures that the agreed-upon standards for performance and reliability are consistently met.

Please note that this list may be subject to updates and changes as new services or URLs are added or modified. It is essential to review and communicate any updates to stakeholders to ensure everyone has the most up-to-date information on the covered services/URLs under the SLA.
