---
description: This page shows you how to scrap logs and push them to our ingestor
---

# Monitoring Logs Of A New System

The provided script automates the deployment and configuration of the Promtail agent, which is used for log aggregation and forwarding to a Loki deployment. Here's a brief summary of how to use the script:

1. Review, download and run the _promtail_ agent installer script:

```
wget https://gitlab.com/PBSA/infrastructure/devops-misc-issues/-/raw/main/scripts/loki/promtail-install.sh
```

2. Add execute permissions and run the script:&#x20;

```
sudo chmod +x promtail-install.sh && ./promtail-install.sh
```

3. System Update: The script begins with a system update, running:  `apt update` and `apt dist-upgrade` to ensure the system is up to date.
4. Promtail Installation: The script then downloads the Promtail binary from the official Grafana Loki repository, extracts it, and moves it to `/usr/local/bin/`. It also verifies the version of Promtail.
5. Configuration: The script creates the necessary directories for Promtail's configuration and data files (`/etc/promtail` and `/data/promtail`). It then adds a default configuration file (`config.yaml`) for Promtail, which you need to edit with your specific settings.
6. Systemd Service: The script sets up a systemd service file for Promtail (`/etc/systemd/system/promtail.service`) to enable automatic start and management of the Promtail agent as a service.
7. Configuration Reminder: The script reminds you to edit the `/etc/promtail/config.yaml` file with your specific configuration details before starting the Promtail agent.
8. Starting and Checking Service: The script provides instructions for starting the Promtail agent using `sudo systemctl start promtail.service` and checking the service's status with `systemctl status promtail.service`.



Remember to review and update the configuration files with your desired settings to ensure Promtail is properly configured for your environment.
