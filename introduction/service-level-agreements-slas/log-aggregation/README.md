---
description: >-
  This page provides an overview of our Log Aggregation Service Level Agreement
  (SLA) that outlines the commitments and expectations related to our log
  aggregation service.
---

# Log Aggregation

Our log aggregation SLA defines the standards and targets for log collection, processing, storage, and accessibility. It serves as a foundation for ensuring the availability, reliability, and performance of the log aggregation service to support the operational needs of our organization.

In this section, you will find information on the key components of our log aggregation SLA, including:

1.  **Log Collection and Ingestion:**

    * Supported log formats:  Plain Text, JSON, or structured logs.
    * Ingestion mechanisms: Log forwarding


2.  **Processing:**

    * Processing time: Immediately


3.  **Storage and Retention:**

    * Data retention period: 90 days
    * Storage capacity: 750GB&#x20;
    * Backup Schedule: Weekly


4.  **Search and Retrieval:**

    * Search capabilities: Plain Text, LogQL, Regex


5.  **Performance and Scalability:**

    * Ingestion rate: 4MB/s per log stream
    * System availability: 99.9%


6. **Monitoring and Alerting:**
   * Metrics and thresholds:
     * &#x20;Storage utilisation, with rocketchat notifications at 80% usage.
   * Alerting mechanism: Rocketchat/Telegram webhook to monitoring-channel
